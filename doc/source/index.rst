.. glare-specs documentation master file

====================
Glare Specifications
====================

Ocata specs:

.. toctree::
   :glob:
   :maxdepth: 1

   specs/ocata/approved/*

==================
Indices and tables
==================

* :ref:`search`
